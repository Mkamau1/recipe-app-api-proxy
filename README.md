# Reccipe App API Proxy 

NGINX proxy app for our recipe app API 

## Usage 

### Environemnt Variable 

* `LISTEN_PORT` - port to listen on (defualt: `8000`)
* `APP_HOST` - Hosname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (defualt: `9000`)